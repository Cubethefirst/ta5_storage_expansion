local S = minetest.get_translator()

-- minetest.register_node("tase:me", {
-- 	description = S("ME Block"),
-- 	tiles = {"ME_ani.png^[verticalframe:32:2"},
-- 	light_source = 5,
-- 	groups = {cracky = 3, stone = 1},
-- 	sounds = default.node_sound_stone_defaults(),
--  })


local PWR_NEEDED = 1
local CYCLE_TIME = 2
local M = minetest.get_meta

local Cable = techage.ElectricCable
--local Cable = techage.Axle
local power = networks.power

local function swap_node(pos, name)
	local node = techage.get_node_lvm(pos)
	if node.name == name then
		return
	end
	node.name = name
	minetest.swap_node(pos, node)
end

local function after_place_node(pos)
	local nvm = techage.get_nvm(pos)
	M(pos):set_string("infotext", "off")
	Cable:after_place_node(pos)
end

local function after_dig_node(pos, oldnode)
	Cable:after_dig_node(pos)
	techage.del_mem(pos)
end

local function on_rightclick(pos, node, clicker)
	local nvm = techage.get_nvm(pos)
	if not nvm.running and power.power_available(pos, Cable) then
		nvm.running = true
		swap_node(pos, "tase:me_core_on")
		M(pos):set_string("infotext", "on")
		minetest.get_node_timer(pos):start(CYCLE_TIME)
	else
		nvm.running = false
		swap_node(pos, "tase:me_core")
		M(pos):set_string("infotext", "off")
		minetest.get_node_timer(pos):stop()
	end
end


local function formspecme()
	return "size[10,12]"..
	default.gui_bg..
	default.gui_bg_img..
	default.gui_slots..
	"textarea[1,0.25;5,0.5;test;Itemsearch;]"..
	"scroll_container[0,1;12,4;test;vertical;]"..
	"scrollbaroptions[min=1;max=120;arrows=hide;]"..
	"list[context;main;0,0;9,12;]"..
	"scroll_container_end[]"..
	"scrollbar[9,0.75;0.25,3.25;vertical;test;0.1]"..
	"list[current_player;craft;1,4.5;3,3;]"..
	"list[current_player;craftpreview;5,5.5;1,1;]"..
	"list[current_player;main;1,8;8,4;]"..
	"listring[current_player;craft]"..
	"listring[context;main]"..
	"listring[current_player;main]"
end

minetest.register_node("tase:dummy", {
	description = S("Dummy Block"),
	tiles = {"ME.png"},
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		inv:set_size('main', 100)
	end,
	after_place_node = function(pos, placer)
		local meta = minetest.get_meta(pos)
		meta:set_string("owner", placer:get_player_name())
		meta:set_string("formspec", formspecme())
	end,
	light_source = 5,
	groups = {cracky = 3, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})


minetest.register_node("tase:me_anim", {
	description = S("Anim ME Block"),
	tiles = {
		{
			name = "ME_animated.png",
			animation = {
				type     = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length   = 2.5,
			}
		}
	},
	light_source = 5,
	groups = {cracky = 3, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("tase:me_core_on", {
	description = S("ME Core"),
	drawtype = "glasslike_framed",
	tiles = {
		{
			name = "me_core_con.png^me_core_over.png",
			animation = {
				type     = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length   = 2.0,
			}
		}
	},

	on_timer = function(pos, elapsed)
		local consumed = power.consume_power(pos, Cable, nil, PWR_NEEDED)
		if consumed < PWR_NEEDED then
			swap_node(pos, "tase:me_core")
		end
		return true
	end,
	on_rightclick = on_rightclick,
	after_place_node = after_place_node,
	after_dig_node = after_dig_node,
	light_source = 5,
	on_rotate = screwdriver.disallow,
	is_ground_content = false,
	groups = {cracky = 3,not_in_creative_inventory = 1},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("tase:me_core", {
	description = S("ME Core"),
	drawtype = "glasslike_framed",
	tiles = {"ME.png","me_core_con_overlay.png"},

	on_timer = function(pos, elapsed)
		local consumed = power.consume_power(pos, Cable, nil, PWR_NEEDED)
		if consumed == PWR_NEEDED then
			swap_node(pos, "tase:me_core_on")
		end
		return true
	end,
	on_rightclick = on_rightclick,
	after_place_node = after_place_node,
	after_dig_node = after_dig_node,
	light_source = 1,
	on_rotate = screwdriver.disallow,
	is_ground_content = false,
	groups = {cracky = 3},
	sounds = default.node_sound_stone_defaults(),
})

power.register_nodes({"tase:me_core", "tase:me_core_on"}, Cable, "con")


minetest.register_node("tase:me_glass_cable0", {
	description = S("ME Glass Cable"),
	tiles = {"me_glass_cable.png"},
    drawtype = "nodebox",
	groups = {cracky = 3},
    paramtype = "light",
    drawtype = "nodebox",
	node_box = {
		type = "connected",
		fixed          = {-2/16, -2/16, -2/16, 2/16,  2/16, 2/16},
		connect_top    = {-2/16, -2/16, -2/16, 2/16,  0.5,  2/16}, -- y+
		connect_bottom = {-2/16, -0.5,  -2/16, 2/16,  2/16, 2/16}, -- y-
		connect_front  = {-2/16, -2/16, -0.5,  2/16,  2/16, 2/16}, -- z-
		connect_back   = {-2/16, -2/16,  2/16, 2/16,  2/16, 0.5 }, -- z+
		connect_left   = {-0.5,  -2/16, -2/16, 2/16,  2/16, 2/16}, -- x-
		connect_right  = {-2/16, -2/16, -2/16, 0.5,   2/16, 2/16}, -- x+
	},
})

